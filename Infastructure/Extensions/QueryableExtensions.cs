﻿using System.Linq;

namespace SimpleBlog.Infastructure.Extensions
{
    public static class QueryableExtensions
    {
        public static IQueryable<T> Paginate<T>(this IQueryable<T> source, int page, int pageSize)
        {
            return source.Skip<T>(page * pageSize).Take<T>(pageSize);
        }
    }
}
