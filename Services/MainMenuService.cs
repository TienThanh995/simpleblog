﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using AutoMapper;

using SimpleBlog.DataAccess.Repositorys.Constracts;
using SimpleBlog.Entities.Dtos;
using SimpleBlog.Entities.Entities;
using SimpleBlog.Services.Constracts;

namespace SimpleBlog.Services
{
    public class MainMenuService : IMainMenuService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        public MainMenuService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        public async Task AddMainMenuAsync(MainMenuDto MainMenuDto)
        {
            var mainMenu = _mapper.Map<MainMenu>(MainMenuDto);
            await _unitOfWork.MainMenuRepository.AddAsync(mainMenu);
        }

        public async Task DeleteMainMenu(MainMenuDto mainMenuDto)
        {
            var mainMenu = _mapper.Map<MainMenu>(mainMenuDto);
            _unitOfWork.MainMenuRepository.Delete(mainMenu);
            await _unitOfWork.SaveAsync();
        }

        public async Task<IEnumerable<MainMenuDto>> GetActiveMainMenu()
        {
            var result = await _unitOfWork.MainMenuRepository.ListAsync(x => x.Id != 0 && x.Status);
            return _mapper.Map<IEnumerable<MainMenu>, IEnumerable<MainMenuDto>>(result);
        }

        public async Task<MainMenuDto> GetMainMenu(int MainMenuId)
        {
            var result = await _unitOfWork.MainMenuRepository.GetByIdAsync(MainMenuId);
            return _mapper.Map<MainMenuDto>(result);
        }

        public async Task<IEnumerable<MainMenuDto>> GetMainMenu()
        {
            var result = await _unitOfWork.MainMenuRepository.ListAsync(x => x.Id != 0);
            return _mapper.Map<IEnumerable<MainMenu>, IEnumerable<MainMenuDto>>(result);
        }

        public async Task UpdateMainMenu(MainMenuDto MainMenu)
        {
            MainMenu.UpdatedDate = DateTime.Now;
             _unitOfWork.MainMenuRepository.Update(_mapper.Map<MainMenu>(MainMenu));
            await _unitOfWork.SaveAsync();
        }
    }
}
