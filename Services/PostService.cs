﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using AutoMapper;

using SimpleBlog.DataAccess.Repositorys.Constracts;
using SimpleBlog.Entities.Dtos;
using SimpleBlog.Entities.Entities;
using SimpleBlog.Infastructure.Helpers;
using SimpleBlog.Services.Constracts;

namespace SimpleBlog.Services
{
    public class PostService : IPostService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        public PostService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task AddPostAsync(PostDto postDto)
        {
            postDto.CreatedDate = DateTime.Now;
            postDto.UpdatedDate = DateTime.Now;
            var post = _mapper.Map<Post>(postDto);
            post.CategoryId = postDto.CategoryId;
            await _unitOfWork.PostRepository.AddAsync(post);
            await _unitOfWork.SaveAsync();
        }

        public async Task DeletePost(PostDto postDto)
        {
             _unitOfWork.PostRepository.Delete(_mapper.Map<Post>(postDto));
            await _unitOfWork.SaveAsync();
        }

        public async Task<PostDto> GetPostById(int postId)
        {
            var post = await _unitOfWork.PostRepository.GetByIdAsync(postId);
            return (_mapper.Map<PostDto>(post));
        }

        public async Task<IEnumerable<PostDto>> GetPosts()
        {
            var post = await _unitOfWork.PostRepository.ListAsync(x => x.Id != 0);
            return _mapper.Map<IEnumerable<Post>, IEnumerable<PostDto>>(post);
        }

        public async Task<PaginatedItems<PostDto>> GetPosts(int pageIndex, int pageSize = 0)
        {
            var paginatedPosts = await _unitOfWork.PostRepository.ListAsyncPage(pageIndex, pageSize, x => x.IsActive, null);
            var data = _mapper.Map<IEnumerable<Post>, IEnumerable<PostDto>>(paginatedPosts.Data);
            return new PaginatedItems<PostDto>(paginatedPosts.PageIndex, paginatedPosts.PageSize, paginatedPosts.Count, data);
        }

        public async Task<PaginatedItems<PostDto>> GetPostByCategoryId(int categoryId, int pageIndex, int pageSize = 0)
        {
            var paginatedPosts = await _unitOfWork.PostRepository.ListAsyncPage(pageIndex, pageSize, x => x.IsActive && x.CategoryId == categoryId, null);
            var responses = _mapper.Map<IEnumerable<Post>, IEnumerable<PostDto>>(paginatedPosts.Data);
            return new PaginatedItems<PostDto>(paginatedPosts.PageIndex, paginatedPosts.PageSize, paginatedPosts.Count, responses);
        }

        public async Task UpdatePost(PostDto post)
        {
            post.UpdatedDate = DateTime.Now;
            _unitOfWork.PostRepository.Update(_mapper.Map<Post>(post));
            await _unitOfWork.SaveAsync();
        }
    }
}
