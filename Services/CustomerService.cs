﻿using System;
using System.Threading.Tasks;

using AutoMapper;
using SimpleBlog.DataAccess.Repositorys.Constracts;

using SimpleBlog.Entities.Dtos;
using SimpleBlog.Entities.Entities;
using SimpleBlog.Services.Constracts;

namespace SimpleBlog.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        public CustomerService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<CustomerDto> AddCustomerAsync(CustomerDto customer)
        {
            var result = new Customer();
            var find = _unitOfWork.CustomerRepository.Find(w => w.Phone == customer.Phone);
            if (find.Result != null)
            {
                if (!find.Result.Email.Equals(customer.Email))
                {
                    find.Result.Email = customer.Email;
                    find.Result.UpdatedDate = DateTime.Now;
                    result = _unitOfWork.CustomerRepository.Update(_mapper.Map<Customer>(customer));                   
                }
                else
                {
                    return customer;
                }
            }
            else
            {
                customer.CreatedDate = DateTime.Now;
                customer.UpdatedDate = DateTime.Now;
                result = await _unitOfWork.CustomerRepository.AddAsync(_mapper.Map<Customer>(customer));
            }
            await _unitOfWork.SaveAsync();
            return _mapper.Map<CustomerDto>(result);
        }
    }
}
