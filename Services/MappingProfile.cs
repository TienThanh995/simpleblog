﻿using AutoMapper;
using SimpleBlog.Entities.Dtos;
using SimpleBlog.Entities.Entities;

namespace SimpleBlog.Services
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<PostDto, Post>();
            CreateMap<MainMenuDto, MainMenu>().ReverseMap();
            CreateMap<CategoryDto, Category>().ReverseMap();
            CreateMap<CustomerDto, Customer>();
            CreateMap<CompanyInfo, CompanyInfoDto>()
                .ForMember(dest => dest.Content, source => source.MapFrom(com => com.CompanyIntro))
                .ReverseMap()
                .ForMember(dest => dest.CompanyIntro, source => source.MapFrom(com => com.Content));
        }
    }
}