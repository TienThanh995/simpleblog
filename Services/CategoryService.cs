﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using AutoMapper;

using SimpleBlog.DataAccess.Repositorys.Constracts;
using SimpleBlog.Entities.Dtos;
using SimpleBlog.Entities.Entities;
using SimpleBlog.Services.Constracts;

namespace SimpleBlog.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        public CategoryService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<CategoryDto> AddCategoryAsync(CategoryDto Category)
        {
           var result = await _unitOfWork.CategoryRepository.AddAsync(_mapper.Map<Category>(Category));
           return _mapper.Map<CategoryDto>(result);
        }

        public Task DeleteCategory(CategoryDto categoryDto)
        {
            var category = _mapper.Map<Category>(categoryDto);
             _unitOfWork.CategoryRepository.Delete(category);
            return _unitOfWork.SaveAsync();
        }

        public async Task<CategoryDto> GetCategory(int categoryId)
        {
            var result = await _unitOfWork.CategoryRepository.GetByIdAsync(categoryId);
            return _mapper.Map<CategoryDto>(result);
        }

        public async Task<IEnumerable<CategoryDto>> GetCategory()
        {
            var list = await _unitOfWork.CategoryRepository.ListAsync(x => x.Id != 0);
            return _mapper.Map<IEnumerable<Category>, IEnumerable<CategoryDto>>(list);
        }

        public async Task<IEnumerable<CategoryDto>> GetCategoryByLaw()
        {
            var list = await _unitOfWork.CategoryRepository.ListAsync(x => x.Id != 0);
            return _mapper.Map<IEnumerable<Category>, IEnumerable<CategoryDto>>(list);
        }

        public async Task<IEnumerable<CategoryDto>> GetCategoryByOperation()
        {
            var list = await _unitOfWork.CategoryRepository.ListAsync(x => x.Id != 0);
            return _mapper.Map<IEnumerable<Category>, IEnumerable<CategoryDto>>(list);
        }

        public async Task UpdateCategory(CategoryDto categoryDto)
        {
            categoryDto.UpdatedDate = DateTime.Now;
            var category = _mapper.Map< Category>(categoryDto);
             _unitOfWork.CategoryRepository.Update(category);
            await _unitOfWork.SaveAsync();
        }
    }
}
