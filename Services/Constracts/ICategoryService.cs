﻿using SimpleBlog.Entities.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SimpleBlog.Services.Constracts
{
    public interface ICategoryService
    {
        Task<CategoryDto> AddCategoryAsync(CategoryDto Category);
        Task UpdateCategory(CategoryDto Category);
        Task DeleteCategory(CategoryDto category);
        Task<CategoryDto> GetCategory(int CategoryId);
        Task<IEnumerable<CategoryDto>> GetCategoryByLaw();
        Task<IEnumerable<CategoryDto>> GetCategoryByOperation();
        Task<IEnumerable<CategoryDto>> GetCategory();
    }
}
