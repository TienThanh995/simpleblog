﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SimpleBlog.Entities.Dtos;
using SimpleBlog.Infastructure.Helpers;

namespace SimpleBlog.Services.Constracts
{
    public interface IPostService
    {
        Task AddPostAsync(PostDto news);
        Task UpdatePost(PostDto news);
        Task DeletePost(PostDto newsDto);
        Task<PostDto> GetPostById(int newsId);
        Task<IEnumerable<PostDto>> GetPosts();
        Task<PaginatedItems<PostDto>> GetPostByCategoryId(int categoryId, int pageIndex, int pageSize = 0);
        Task<PaginatedItems<PostDto>> GetPosts(int pageIndex, int pageSize = 0);
    }
}
