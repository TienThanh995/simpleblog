﻿using SimpleBlog.Entities.Dtos;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SimpleBlog.Services.Constracts
{
    public interface IMainMenuService
    {
        Task AddMainMenuAsync(MainMenuDto MainMenu);
        Task UpdateMainMenu(MainMenuDto MainMenu);
        Task DeleteMainMenu(MainMenuDto MainMenuId);
        Task<MainMenuDto> GetMainMenu(int MainMenuId);
        Task<IEnumerable<MainMenuDto>> GetMainMenu();
        Task<IEnumerable<MainMenuDto>> GetActiveMainMenu();
    }
}