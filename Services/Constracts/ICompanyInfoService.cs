﻿using SimpleBlog.Entities.Dtos;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace SimpleBlog.Services.Constracts
{
    public interface ICompanyInfoService
    {
        Task<CompanyInfoDto> AddCompanyInfoAsync(CompanyInfoDto CompanyInfo);
        Task<CompanyInfoDto> UpdateCompanyInfo(CompanyInfoDto CompanyInfo);
        Task DeleteCompanyInfo(CompanyInfoDto CompanyInfoDto);
        Task<CompanyInfoDto> GetCompanyInfo(int CompanyInfoId);
        Task<IEnumerable<CompanyInfoDto>> GetCompanyInfo();
        Task<CompanyInfoDto> GetFirstCompany();
    }
}
