﻿using SimpleBlog.Entities.Dtos;
using System.Threading.Tasks;

namespace SimpleBlog.Services.Constracts
{
    public interface ICustomerService
    {
        Task<CustomerDto> AddCustomerAsync(CustomerDto customer);
    }
}
