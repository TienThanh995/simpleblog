﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using AutoMapper;

using SimpleBlog.DataAccess.Repositorys.Constracts;
using SimpleBlog.Entities.Dtos;
using SimpleBlog.Entities.Entities;
using SimpleBlog.Services.Constracts;

namespace SimpleBlog.Services
{
    public class CompanyInfoService : ICompanyInfoService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        public CompanyInfoService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        public async Task<CompanyInfoDto> AddCompanyInfoAsync(CompanyInfoDto companyInfo)
        {
           var result = await _unitOfWork.CompanyInfoRepository.AddAsync(_mapper.Map<CompanyInfo>(companyInfo));
           return _mapper.Map<CompanyInfoDto>(result);
        }

        public Task DeleteCompanyInfo(CompanyInfoDto companyInfoDto)
        {
            _unitOfWork.CompanyInfoRepository.Delete(_mapper.Map<CompanyInfo>(companyInfoDto));
            return _unitOfWork.SaveAsync();
        }

        public async Task<CompanyInfoDto> GetCompanyInfo(int companyInfoId)
        {
            var companies = await _unitOfWork.CompanyInfoRepository.Find(x => x.Id == companyInfoId);
            return _mapper.Map<CompanyInfoDto>(companies);
        }

        public async Task<IEnumerable<CompanyInfoDto>> GetCompanyInfo()
        {
            var companies = await _unitOfWork.CompanyInfoRepository.ListAsync(x => x.Id != 0);
            return _mapper.Map<IEnumerable<CompanyInfo>, IEnumerable<CompanyInfoDto>>(companies);
        }

        public async Task<CompanyInfoDto> GetFirstCompany()
        {
            var company = await _unitOfWork.CompanyInfoRepository.Find(x => x.Id != 0);
            return _mapper.Map <CompanyInfoDto> (company);            
        }

        public async Task<CompanyInfoDto> UpdateCompanyInfo(CompanyInfoDto companyInfo)
        {
            companyInfo.UpdatedDate = DateTime.Now;
            var result = _unitOfWork.CompanyInfoRepository.Update(_mapper.Map<CompanyInfo>(companyInfo));
            await _unitOfWork.SaveAsync();
            return _mapper.Map<CompanyInfoDto>(result);
        }
    }
}
