FROM microsoft/dotnet:2.1-sdk AS build
WORKDIR /app

# copy csproj and restore as distinct layers
COPY *.sln .
COPY LawOffice/*.csproj ./Services/LawOffice.Services.csproj
COPY LawOffice/*.csproj ./Infastructure/LawOffice.Infastructure.csproj
COPY LawOffice/*.csproj ./Entities/LawOffice.Entities.csproj
COPY LawOffice/*.csproj ./DataAccess/LawOffice.DataAccess.csproj
COPY LawOffice/*.csproj ./LawOffice/
RUN dotnet restore

# copy everything else and build app
COPY LawOffice/. ./LawOffice/
WORKDIR /app/LawOffice
RUN dotnet publish -c Release -o out


FROM microsoft/dotnet:2.1-aspnetcore-runtime AS runtime
WORKDIR /app
COPY --from=build /app/LawOffice/out ./
ENTRYPOINT ["dotnet", "LawOffice.dll"]
