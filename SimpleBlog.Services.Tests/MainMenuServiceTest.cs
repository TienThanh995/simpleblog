using AutoMapper;
using Moq;
using SimpleBlog.DataAccess.Repositorys.Constracts;
using SimpleBlog.Entities.Dtos;
using SimpleBlog.Entities.Entities;
using SimpleBlog.Services.Constracts;
using System;
using System.Threading.Tasks;
using Xunit;

namespace SimpleBlog.Services.Tests
{
    public class MainMenuServiceTest
    {
        private MainMenu mainMenu;
        private MainMenuDto mainMenuDto;
        private readonly Mock<IMapper> _mapperMock;
        private readonly Mock<IUnitOfWork> _unitOfWork;
        private readonly IMainMenuService _mainMenuService;
        public MainMenuServiceTest()
        {
            _mapperMock = new Mock<IMapper>();
            _unitOfWork = new Mock<IUnitOfWork>();
            _mainMenuService = new MainMenuService(_unitOfWork.Object, _mapperMock.Object);
            SetUp();
        }

        // Arrange
        void SetUp()
        {
            mainMenu = new MainMenu()
            {
                Id = 1,
                DisplayOrder = 1,
                Title = "Test Menu",
                IsParent = false,
                Link = "link.com",
                Status = true,
                Target = "1",
                CreatedDate = DateTime.Now,
                UpdatedDate = DateTime.Now
            };
            mainMenuDto = new MainMenuDto()
            {
                Id = 1,
                DisplayOrder = 1,
                Title = "Test Menu",
                IsParent = false,
                Link = "link.com",
                Status = true,
                Target = "1",
                CreatedDate = DateTime.Now,
                UpdatedDate = DateTime.Now
            };

            _unitOfWork.Setup(x => x.MainMenuRepository.GetByIdAsync(It.IsAny<int>())).ReturnsAsync(mainMenu);
            _mapperMock.Setup(x => x.Map<MainMenuDto>(It.IsAny<MainMenu>())).Returns(mainMenuDto);
        }
        [Fact]
        public async Task GetMainMenuById_ReturnMainMenu_WhenSuccess()
        {
            // Act
            var result = await _mainMenuService.GetMainMenu(1);

            //Assert
            Assert.Equal("Test Menu", result.Title);
        }


    }
}
