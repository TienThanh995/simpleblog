using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Moq;
using SimpleBlog.Areas.Admin.Controllers;
using SimpleBlog.Entities.Dtos;
using SimpleBlog.Entities.Entities;
using SimpleBlog.Services.Constracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace SimpleBlog.Controller.Tests
{
    public class CategoryControllerTests
    {
        private readonly Mock<ICategoryService> mockCategoryService;
        CategoryController controller;
        public CategoryControllerTests()
        {
            mockCategoryService = new Mock<ICategoryService>();
            controller = new CategoryController(mockCategoryService.Object);
        }
        
        private List<CategoryDto> GetTestCategory()
        {
            return new List<CategoryDto>
            {
                new CategoryDto { Id = 1, CreatedDate = DateTime.Now, Slug = "programming", Title = "Programming"},
                new CategoryDto { Id = 2, CreatedDate = DateTime.Now, Slug = "bussiness", Title = "Bussiness"}
            };
        }
        private List<PostDto> GetTestPost()
        {
            return new List<PostDto>
            {
                new PostDto { Id = 1, CategoryId = 1, Content = "Post test", Excerpt = "post test", Title = "First post test", IsActive = true, Slug = "first-post-test"},
                new PostDto { Id = 2, CategoryId = 1, Content = "Programing Post test", Excerpt = "post test", Title = "Second post test", IsActive = true, Slug = "second-post-test"},
                new PostDto { Id = 3, CategoryId = 2, Content = "Bussiness Post test", Excerpt = "post test", Title = "Third post test", IsActive = true, Slug = "third-post-test"}
            };
        }

        [Fact]
        public async Task Index_ReturnsAViewResult_WithAListOfPosts()
        {
            // Arrange
            mockCategoryService.Setup(x => x.GetCategory()).ReturnsAsync(GetTestCategory());

            // Act
            var result = await controller.Index();

            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsAssignableFrom<IEnumerable<CategoryDto>>(
                viewResult.ViewData.Model);
            Assert.Equal(2, model.Count());

        }

        [Fact]
        public async Task IndexPost_ReturnsBadRequestResult_WhenModelStateIsInvalid()
        {
            // Arrange
            mockCategoryService.Setup(x => x.GetCategory()).ReturnsAsync(GetTestCategory());

            controller.ModelState.AddModelError("Title", "Required");
            var newPost = new CategoryDto();

            // Act
            var result = await controller.Create(newPost);

            // Assert
            var badRequestResult = Assert.IsType<BadRequestObjectResult>(result);
            Assert.IsType<SerializableError>(badRequestResult.Value);
        }

        [Fact]
        public async Task IndexPost_ReturnsARedirectAndAddsPost_WhenModelStateIsValid()
        {
            var post = new CategoryDto
            {
                Id = 1,
                Title = "Unit test for first post"
            };
            // Arrange
            mockCategoryService.Setup(repo => repo.AddCategoryAsync(It.IsAny<CategoryDto>()))
                .Returns(Task.FromResult(post));

            // Act
            var result = await controller.Create(post);

            // Assert
            var redirectToActionResult = Assert.IsType<JsonResult>(result);
            var jsonResult = (CategoryDto)redirectToActionResult.Value;
            Assert.Equal(post.Title, jsonResult.Title);
            mockCategoryService.Verify();
        }
    }
}
