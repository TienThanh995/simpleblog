﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using SimpleBlog.Components;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SimpleBlog.Controller.Tests
{
    public class FooterComponentTest
    {
        [Fact]
        public async Task RunInvokeAsyncFooterComponent()
        {

            // Arrange
            var httpContext = new DefaultHttpContext();

            var viewContext = new ViewContext
            {
                HttpContext = httpContext
            };
            var viewComponentContext = new ViewComponentContext();
            viewComponentContext.ViewContext = viewContext;

            var viewComponent = new FooterViewComponent
            {
                ViewComponentContext = viewComponentContext
            };

            //Act
            var result = await viewComponent.InvokeAsync();

            // Assert
            Assert.IsAssignableFrom<IViewComponentResult>(result);
        }
    }
}
