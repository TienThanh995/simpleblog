﻿using SimpleBlog.Entities.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace SimpleBlog.DataAccess.EFs
{
    public class SimpleBlogContext : DbContext
    {
        private readonly IConfiguration _config;

        public SimpleBlogContext(IConfiguration config, DbContextOptions<SimpleBlogContext> options)
            : base(options)
        {
            _config = config;
        }

        public DbSet<Post> News { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<MainMenu> MainMenus { get; set; }
        public DbSet<CompanyInfo> CompanyInfos { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_config.GetConnectionString("SimpleBlogConnection"), options =>
            {
                options.MigrationsHistoryTable("__UsersMigrationsHistory", "SimpleBlog");
            });
        }
    }
}
