﻿using System;
using System.Threading.Tasks;
using SimpleBlog.DataAccess.EFs;
using SimpleBlog.DataAccess.Repositorys.Constracts;
using SimpleBlog.Entities.Entities;

namespace SimpleBlog.DataAccess.Repositorys
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private readonly SimpleBlogContext _context;
        private IEfRepository<Post> _postRepository;
        private IEfRepository<Category> _categoryRepository;
        private IEfRepository<MainMenu> _mainMenuRepository;
        private IEfRepository<Customer> _customerRepository;
        private IEfRepository<CompanyInfo> _companyInfoRepository;
        public UnitOfWork(SimpleBlogContext context)
        {
            _context = context;
        }

        public IEfRepository<Category> CategoryRepository
        {
            get
            {
                return _categoryRepository = _categoryRepository ?? new GenericRepository<Category>(_context);
            }
        }
        public IEfRepository<MainMenu> MainMenuRepository
        {
            get
            {
                return _mainMenuRepository = _mainMenuRepository ?? new GenericRepository<MainMenu>(_context);
            }
        }
        public IEfRepository<Customer> CustomerRepository
        {
            get
            {
                return _customerRepository = _customerRepository ?? new GenericRepository<Customer>(_context);
            }
        }
        public IEfRepository<CompanyInfo> CompanyInfoRepository
        {
            get
            {
                return _companyInfoRepository = _companyInfoRepository ?? new GenericRepository<CompanyInfo>(_context);
            }
        }

        public IEfRepository<Post> PostRepository
        {
            get
            {
                return _postRepository = _postRepository ?? new GenericRepository<Post>(_context);
            }
        }

        public void Dispose()
        {
            _context.Dispose();
            GC.SuppressFinalize(this);
        }

        public Task SaveAsync()
        {
            return _context.SaveChangesAsync();
        }
    }
}
