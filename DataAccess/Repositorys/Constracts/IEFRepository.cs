﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using SimpleBlog.Infastructure.Helpers;
using Microsoft.EntityFrameworkCore.Query;

namespace SimpleBlog.DataAccess.Repositorys.Constracts
{
    public interface IEfRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        Task<TEntity> GetByIdAsync(int id, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> includeProperties);

        Task<TEntity> GetByCompositeKeyAsync(int id, Decimal version, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> includeProperties);

        Task<TEntity> Find(Expression<Func<TEntity, bool>> filter, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> includeProperties = null);

        Task<bool> Any(Expression<Func<TEntity, bool>> filter, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> includeProperties = null);

        Task<IEnumerable<TEntity>> ListAsync(Expression<Func<TEntity, bool>> filter, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> includeProperties);

        Task<PaginatedItems<TEntity>> ListAsyncPage(int pageIndex, int pageSize, Expression<Func<TEntity, bool>> filter, Func<IQueryable<TEntity> , IOrderedQueryable<TEntity>> orderBy = null, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> includeProperties = null);

    }
}
