﻿using SimpleBlog.Entities.Entities;
using System.Threading.Tasks;

namespace SimpleBlog.DataAccess.Repositorys.Constracts
{
    public interface IUnitOfWork
    {
        IEfRepository<Post> PostRepository { get; }
        IEfRepository<Category> CategoryRepository { get; }
        IEfRepository<MainMenu> MainMenuRepository { get; }
        IEfRepository<Customer> CustomerRepository { get; }
        IEfRepository<CompanyInfo> CompanyInfoRepository { get; }

        Task SaveAsync();
    }
}
