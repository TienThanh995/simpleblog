﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using SimpleBlog.DataAccess.Repositorys.Constracts;
using SimpleBlog.Entities;
using SimpleBlog.Infastructure.Extensions;
using SimpleBlog.Infastructure.Helpers;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Query;

namespace SimpleBlog.DataAccess.Repositorys
{
    public class GenericRepository<TEntity> : IEfRepository<TEntity> where TEntity : BaseEntity
    {
        private readonly DbContext _context;
        private readonly DbSet<TEntity> _dbSet;

        public GenericRepository(DbContext context)
        {
            var dbContext = context;
            _context = dbContext ?? throw new ArgumentNullException(nameof(context));
            _dbSet = _context.Set<TEntity>();
        }

        public virtual async Task<TEntity> AddAsync(TEntity entity)
        {
            _context.Entry(entity);
            EntityEntry<TEntity> entityEntry = await _dbSet.AddAsync(entity, new CancellationToken()); 
            return entityEntry.Entity;
        }

        public virtual void Delete(TEntity entity)
        {
            _context.Entry(entity).State = EntityState.Deleted;
        }

        public IQueryable<TEntity> GetWithInclude(Expression<Func<TEntity, bool>> predicate, params string[] include)
        {
            IQueryable<TEntity> query = _dbSet;
            query = include.Aggregate(query, (current, inc) => current.Include(inc));
            return query.Where(predicate);
        }

        public virtual async Task<TEntity> GetByIdAsync(int id)
        {
            return await _dbSet.AsNoTracking().SingleOrDefaultAsync(e => e.Id.Equals(id));
        }

        public virtual async Task<TEntity> GetByIdAsync(int id, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> includeProperties)
        {
            IQueryable<TEntity> source = _dbSet.AsNoTracking();
            if (includeProperties != null)
                source = includeProperties(source);
            return await source.SingleOrDefaultAsync(e => e.Id.Equals(id));
        }

        public virtual async Task<TEntity> GetByCompositeKeyAsync(int id, Decimal version, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> includeProperties)
        {
            IQueryable<TEntity> source = _dbSet.AsNoTracking();
            if (includeProperties != null)
                source = includeProperties(source);
            return await source.SingleOrDefaultAsync(e => e.Id == id);
        }

        public virtual async Task<TEntity> Find(Expression<Func<TEntity, bool>> filter, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> includeProperties = null)
        {
            IQueryable<TEntity> source = _dbSet.AsNoTracking();
            if (filter != null)
                source = source.Where(filter);
            if (includeProperties != null)
                source = includeProperties(source);
            return await source.FirstOrDefaultAsync();
        }

        public async Task<bool> Any(Expression<Func<TEntity, bool>> filter, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> includeProperties = null)
        {
            IQueryable<TEntity> source = _dbSet.AsNoTracking();
            if (includeProperties != null)
                source = includeProperties(source);
            if (filter != null)
                return await source.AnyAsync(filter);
            return await source.AnyAsync();
        }

        public virtual async Task<IEnumerable<TEntity>> ListAsync(Expression<Func<TEntity, bool>> filter, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> includeProperties)
        {
            IQueryable<TEntity> source = _dbSet.AsNoTracking();
            if (filter != null)
                source = source.Where(filter);
            if (includeProperties != null)
                source = includeProperties(source);
            if (orderBy != null)
                return await orderBy(source).ToListAsync();
            return await source.ToListAsync();
        }

        public virtual async Task<IReadOnlyList<TEntity>> ListAsync(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null)
        {
            IQueryable<TEntity> source = _dbSet.AsNoTracking();
            if (filter != null)
                source = source.Where(filter);
            if (orderBy != null)
                return await orderBy(source).ToListAsync();
            return await source.ToListAsync();
        }

        public virtual async Task<PaginatedItems<TEntity>> ListAsyncPage(int pageIndex, int pageSize, Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> includeProperties = null)
        {
            IQueryable<TEntity> source = _dbSet.AsNoTracking();
            if (filter != null)
                source = source.Where(filter);
            if (includeProperties != null)
                source = includeProperties(source);
            int projectTotal = source.Count();
            List<TEntity> listAsync;
            if (orderBy != null)
                listAsync = await orderBy(source).Paginate(pageIndex, pageSize).ToListAsync();
            else
                listAsync = await source.OrderByDescending( x=>x.UpdatedDate).Paginate(pageIndex, pageSize).ToListAsync();
            return new PaginatedItems<TEntity>(pageIndex, pageSize, projectTotal, listAsync);
        }

        public virtual TEntity Update(TEntity entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
            _context.Entry(entity).Property(x => x.CreatedDate).IsModified = false;
            return entity;
        }

        public virtual async Task<int> GetLastInsertedId()
        {
            return await _dbSet.Select(r => r.Id).DefaultIfEmpty(0).MaxAsync();
        }
    }
}

/*
  remove after check the above function work smothly
    internal SimpleBlogContext _context;
        internal DbSet<TEntity> _dbSet;

        public GenericRepository(SimpleBlogContext context)
        {
            _context = context;
            _dbSet = context.Set<TEntity>();
        }
        private void Delete(TEntity entityToDelete)
        {
            if (_context.Entry(entityToDelete).State == EntityState.Detached)
            {
                _dbSet.Attach(entityToDelete);
            }
            _dbSet.Remove(entityToDelete);
        }

        public async Task<IEnumerable<TEntity>> GetAsync()
        {
            return await _context.Set<TEntity>().ToListAsync();
        }

        public TEntity GetById(int id)
        {
            return _dbSet.Find(id);
        }

        public IEnumerable<TEntity> GetMany(Func<TEntity, bool> where)
        {
            return _dbSet.Where(where).ToList();
        }

        public IQueryable<TEntity> GetWithInclude(Expression<Func<TEntity, bool>> predicate, params string[] include)
        {
            IQueryable<TEntity> query = _dbSet;
            query = include.Aggregate(query, (current, inc) => current.Include(inc));
            return query.Where(predicate);
        }

        public async Task<EntityEntry<TEntity>> InsertAsync(TEntity entity)
        {
            return await _dbSet.AddAsync(entity);
        }

        public void Update(TEntity entityToUpdate)
        {
            _dbSet.Attach(entityToUpdate);
            _context.Entry(entityToUpdate).State = EntityState.Modified;
        }
        public virtual void Delete(object id)
        {
            TEntity entityToDelete = _dbSet.Find(id);
            Delete(entityToDelete);
        }
*/