﻿using System.Collections.Generic;

namespace SimpleBlog.Entities.Dtos
{
    public class MainMenuDto : BaseEntity
    {
        public string Title { get; set; }
        public string Link { get; set; }
        public int DisplayOrder { get; set; }
        public string Target { get; set; }
        public bool Status { get; set; }
        public bool IsParent { get; set; }
    }
}
