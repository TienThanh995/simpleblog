﻿using System.ComponentModel.DataAnnotations;

namespace SimpleBlog.Entities.Dtos
{
    public class CategoryDto : BaseEntity
    {
        [Required]
        public string Title { get; set; }
        public string Slug { get; set; }
    }
}
