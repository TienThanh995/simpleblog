﻿using System.ComponentModel.DataAnnotations;

namespace SimpleBlog.Entities.Dtos
{
    public class CustomerDto : BaseEntity
    {
        [Required(ErrorMessage = "Vui lòng nhập họ tên")]
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Vui lòng nhập số điện thoại")]
        [Display(Name = "Phone Number")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^(\+\s?)?((?<!\+.*)\(\+?\d+([\s\-\.]?\d+)?\)|\d+)([\s\-\.]?(\(\d+([\s\-\.]?\d+)?\)|\d+))*(\s?(x|ext\.?)\s?\d+)?$", ErrorMessage = "Vui lòng nhập số điện thoại hợp lệ!")]
        public string Phone { get; set; }
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}