﻿
namespace SimpleBlog.Entities.Dtos
{
    public class CustomerQuestion
    {
        public CustomerDto CustomerDto { get; set; }
        public string CategoryCode { get; set; }
        public object CategoryList { get; set; }
    }
}