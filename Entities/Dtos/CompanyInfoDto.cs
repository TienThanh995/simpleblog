﻿using System.ComponentModel;

namespace SimpleBlog.Entities.Dtos
{
    public class CompanyInfoDto : BaseEntity
    {
        [DisplayName("Tên công ty")]
        public string CompanyName { get; set; }
        [DisplayName("Giới thiệu công ty")]
        public string Content { get; set; }
        [DisplayName("Tiêu đề SEO")]
        public string MetaTitle { get; set; }
        [DisplayName("Số điện thoại công ty")]
        public int CompanyPhone { get; set; }
        [DisplayName("Email công ty")]
        public string CompanyEmail { get; set; }
        [DisplayName("Đường dẫn facebook")]
        public string CompanyFaceBookUrl { get; set; }
    }
}
