﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SimpleBlog.Entities.Dtos
{
    public class PostDto : BaseEntity
    {
        [DisplayName("Tiêu đề")]
        [Required]
        public string Title { get; set; }
        [DisplayName("Nội dung")]
        public string Content { get; set; }
        public string ImageUrl { get; set; }
        [DisplayName("Đoạn trích")]
        public string Excerpt { get; set; }
        public bool IsActive { get; set; }
        public string Slug { get; set; }
        public int CategoryId { get; set; }
    }
}
