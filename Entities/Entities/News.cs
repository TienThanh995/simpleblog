﻿namespace SimpleBlog.Entities.Entities
{
    public class Post : BaseEntity
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public string ImageUrl { get; set; }
        public string Excerpt { get; set; }
        public bool IsActive { get; set; }
        public string Slug { get; set; }
        public virtual Category Category { get; set; }
        public int CategoryId { get; set; }
    }
}
