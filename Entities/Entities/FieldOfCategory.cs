﻿using System.Collections.Generic;

namespace SimpleBlog.Entities.Entities
{
    public class Category : BaseEntity
    {
        public string Title { get; set; }
        public string Slug { get; set; }
        public virtual ICollection<Post> News { get; set; }
    }
}
