﻿namespace SimpleBlog.Entities.Entities
{
    public class CompanyInfo : BaseEntity
    {
        public string CompanyName { get; set; }
        public string CompanyIntro { get; set; }
        public string MetaTitle { get; set; }
        public int CompanyPhone { get; set; }
        public string CompanyEmail { get; set; }
        public string CompanyFaceBookUrl { get; set; }
    }
}
