﻿using System.Threading.Tasks;

namespace SimpleBlog.Areas.Admin.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
