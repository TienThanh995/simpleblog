﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SimpleBlog.Entities.Dtos;
using SimpleBlog.Services.Constracts;

namespace SimpleBlog.Areas.Admin.Controllers
{
    [Authorize]
    [Area("Admin")]
    [Route("[area]/[controller]/[action]")]
    public class CategoryController : Controller
    {
        private readonly ICategoryService _categoryService;
        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            try
            {
                var result = await _categoryService.GetCategory();
                return View((List<CategoryDto>)result);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return StatusCode(500);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Raw()
        {
            try
            {
                var result = await _categoryService.GetCategory();
                var data = new JsonResult((List<CategoryDto>)result);
                dynamic response = new
                {
                    Data = (List<CategoryDto>)result,
                    Draw = "1",
                    RecordsFiltered = ((List<CategoryDto>)result).Count,
                    RecordsTotal = ((List<CategoryDto>)result).Count
                };
                return Ok(response);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return StatusCode(500);
            }
        }

        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([FromBody]CategoryDto category)
        {
            try
            {
                CategoryDto result = null;
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);
                else
                {
                    category.CreatedDate = DateTime.Now;
                    category.UpdatedDate = DateTime.Now;
                    result = await _categoryService.AddCategoryAsync(category);                 
                }
                return new JsonResult(result);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return BadRequest();
            }
        }

        public async Task<IActionResult> Update(int? Id)
        {
            if (Id == null) return NotFound();
            var category = await _categoryService.GetCategory(Id.Value);
            if (category == null) return NotFound();
            return View(category);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int id, [FromBody]CategoryDto category)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    category.UpdatedDate = DateTime.Now;
                    await _categoryService.UpdateCategory(category);
                    return new JsonResult(category);
                }
                return View(category);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return BadRequest();
            }
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                var category = await _categoryService.GetCategory(id.Value);
                if (category == null)
                {
                    return NotFound();
                }
                await _categoryService.DeleteCategory(category);
                return new JsonResult(new { result = "Success" });
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}