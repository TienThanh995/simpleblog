﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net.Http.Headers;

using SimpleBlog.Entities.Dtos;
using SimpleBlog.Services.Constracts;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;

namespace SimpleBlog.Areas.Admin.Controllers
{
    [Authorize]
    [Area("Admin")]
    [Route("[area]/[controller]/[action]")]
    public class PostController : Controller
    {
        private readonly IHostingEnvironment _environment;
        private readonly IPostService _postService;
        private readonly ICategoryService _categoryService;
        public PostController(IHostingEnvironment environment, IPostService postService, ICategoryService categoryService)
        {
            _categoryService = categoryService;
            _environment = environment;
            _postService = postService;
        }
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            try
            {
                var result = await _postService.GetPosts();
                return View((List<PostDto>)result);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<IActionResult> Create()
        {
            var categories = await _categoryService.GetCategory();
            ViewData["Categories"] = categories;
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(PostDto post)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            else
            {
                var imageUrl = await Upload();
                if (!string.IsNullOrEmpty(imageUrl))
                    post.ImageUrl = $"~/{imageUrl}";
                await _postService.AddPostAsync(post);
            }
            return RedirectToAction(nameof(Index));

        }

        public async Task<IActionResult> Update(int? Id)
        {
            if (Id == null) return NotFound();
            var post = await _postService.GetPostById(Id.Value);
            if (post == null) return NotFound();
            var categories = await _categoryService.GetCategory();
            ViewData["Categories"] = categories;
            return View(post);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int id, PostDto post)
        {
            if (ModelState.IsValid)
            {
                var imageUrl = await Upload();
                if (!string.IsNullOrEmpty(imageUrl))
                    post.ImageUrl = $"~/{imageUrl}";
                post.UpdatedDate = DateTime.Now;
                await _postService.UpdatePost(post);
                return RedirectToAction(nameof(Index));
            }
            return View(post);
        }

        public async Task<IActionResult> Preview(int? Id)
        {
            if (Id == null)
            {
                return NotFound();
            }
            var post = await _postService.GetPostById(Id.Value);
            if (post == null)
            {
                return NotFound();
            }
            return View(post);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var post = await _postService.GetPostById(id.Value);
            if (post == null)
            {
                return NotFound();
            }

            return View(post);
        }

        // POST: Categorycs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var post = await _postService.GetPostById(id);
            await _postService.DeletePost(post);
            return RedirectToAction(nameof(Index));
        }

        private async Task<string> Upload()
        {
            if (HttpContext.Request.Form.Files == null) return null;
            var files = HttpContext.Request.Form.Files;
            string fileName = string.Empty;
            foreach (var file in files)
            {
                if (file.Length > 0)
                {
                    fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');

                    var myUniqueFileName = Convert.ToString(Guid.NewGuid());

                    var fileExtension = Path.GetExtension(fileName);

                    var newFileName = myUniqueFileName + fileExtension;

                    fileName = Path.Combine(_environment.WebRootPath, @"images/postImage") + $@"/{newFileName}";

                    using (FileStream fs = System.IO.File.Create(fileName))
                    {
                        await file.CopyToAsync(fs);
                        fs.Flush();
                    }
                }
                else return null;
            }

            var index = fileName.IndexOf("images", StringComparison.Ordinal);
            var result = fileName.Substring(index);
            return result;
        }
    }
}