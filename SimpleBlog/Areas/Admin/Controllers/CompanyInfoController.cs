﻿using SimpleBlog.Entities.Dtos;
using SimpleBlog.Services.Constracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace SimpleBlog.Areas.Admin.Controllers
{
    [Authorize]
    [Area("Admin")]
    [Route("[area]/[controller]/[action]")]
    public class CompanyInfoController : Controller
    {
        private readonly ICompanyInfoService _companyService;
        public CompanyInfoController(ICompanyInfoService companyService)
        {
            _companyService = companyService;
        }
        public async Task<IActionResult> Index()
        {
            var company = await _companyService.GetFirstCompany();
            return View(company);
        }
        [HttpPost]
        public async Task<IActionResult> Save([FromBody] CompanyInfoDto company)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (company.Id != 0)
                    {
                        await _companyService.UpdateCompanyInfo(company);
                    }
                    else await _companyService.AddCompanyInfoAsync(company);
                }
                return RedirectToAction(nameof(Index));
            }
            catch (System.Exception)
            {
                throw;
            }
        }

    }
}