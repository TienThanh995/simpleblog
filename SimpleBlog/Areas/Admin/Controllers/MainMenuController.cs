﻿using SimpleBlog.Entities.Dtos;
using SimpleBlog.Services.Constracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SimpleBlog.Areas.Admin.Controllers
{
    [Authorize]
    [Area("Admin")]
    [Route("[area]/[controller]/[action]")]
    public class MainMenuController : Controller
    {
        private readonly IMainMenuService _mainMenuService;
        public MainMenuController(IMainMenuService mainMenuService)
        {
            _mainMenuService = mainMenuService;
        }
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            try
            {
                var result = await _mainMenuService.GetMainMenu();
                return View((List<MainMenuDto>)result);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return StatusCode(500);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Raw()
        {
            try
            {
                var result = await _mainMenuService.GetMainMenu();
                var data = new JsonResult((List<MainMenuDto>)result);
                dynamic response = new
                {
                    Data = (List<MainMenuDto>)result,
                    Draw = "1",
                    RecordsFiltered = ((List<MainMenuDto>)result).Count,
                    RecordsTotal = ((List<MainMenuDto>)result).Count
                };
                return Ok(response);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return StatusCode(500);
            }
        }

        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([FromBody]MainMenuDto mainMenu)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    mainMenu.CreatedDate = DateTime.Now;
                    mainMenu.UpdatedDate = DateTime.Now;
                    await _mainMenuService.AddMainMenuAsync(mainMenu);
                    return new JsonResult(mainMenu);
                }
                return View(mainMenu);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return BadRequest();
            }

        }

        public async Task<IActionResult> Update(int? Id)
        {
            if (Id == null) return NotFound();
            var mainMenu = await _mainMenuService.GetMainMenu(Id.Value);
            if (mainMenu == null) return NotFound();
            return View(mainMenu);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(int id, [FromBody]MainMenuDto mainMenu)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    mainMenu.UpdatedDate = DateTime.Now;
                    await _mainMenuService.UpdateMainMenu(mainMenu);
                    return new JsonResult(mainMenu);
                }
                return View(mainMenu);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return BadRequest();
            }
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(int? id)
        {
            try
            {
                if (id == null)
                {
                    return NotFound();
                }

                var mainMenu = await _mainMenuService.GetMainMenu(id.Value);
                if (mainMenu == null)
                {
                    return NotFound();
                }
                await _mainMenuService.DeleteMainMenu(mainMenu);
                return new JsonResult(new { result = "Success" });
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}