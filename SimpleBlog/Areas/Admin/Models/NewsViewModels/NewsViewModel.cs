﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using SimpleBlog.Entities.Dtos;

namespace SimpleBlog.Areas.Admin.Models.NewsViewModels
{
    public class NewsViewModel
    {
        public PostDto NewsDto { get; set; }
        public List<object> Files  { get; set; }
    }
}
