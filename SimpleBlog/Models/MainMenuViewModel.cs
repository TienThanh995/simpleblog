﻿using SimpleBlog.Entities.Dtos;

using System.Collections.Generic;


namespace SimpleBlog.Models
{
    public class MainMenuViewModel
    {
        public IEnumerable<MainMenuDto> MainMenu { get; set; }
        public IEnumerable<CategoryDto> CategoryDtos { get; set; }
    }
}
