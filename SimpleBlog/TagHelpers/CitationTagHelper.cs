﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System.Text;

namespace SimpleBlog.TagHelpers
{
    [HtmlTargetElement("citation")]
    public class CitationTagHelper : TagHelper
    {
        public string Content { get; set; }
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "Citation";
            output.TagMode = TagMode.StartTagAndEndTag;

            var sb = new StringBuilder();
            sb.AppendFormat("<blockquote>{0}</blockquote>", this.Content);

            output.PreContent.SetHtmlContent(sb.ToString());
        }
    }
}
