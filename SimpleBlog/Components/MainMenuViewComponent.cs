﻿using SimpleBlog.Models;
using SimpleBlog.Services.Constracts;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace SimpleBlog.Components
{
    public class MainMenuViewComponent : ViewComponent
    {
        private ICategoryService _categoryService;
        private IMainMenuService _mainMenuService;
        public MainMenuViewComponent(IMainMenuService mainMenuService, ICategoryService CategoryService)
        {
            _mainMenuService = mainMenuService;
            _categoryService = CategoryService;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var categorys = await _categoryService.GetCategory();
            var mainMenus = await _mainMenuService.GetActiveMainMenu();
            var returnData = new MainMenuViewModel()
            {
                MainMenu = mainMenus,
                CategoryDtos = categorys
            };
            return View(returnData);
        }
    }
}
