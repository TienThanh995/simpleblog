﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace SimpleBlog.Components
{
    public class FooterViewComponent : ViewComponent
    {
        public FooterViewComponent() { }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            await Task.CompletedTask;
            return View();
        }
    }
}
