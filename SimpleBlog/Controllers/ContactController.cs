﻿using System.Linq;
using System.Threading.Tasks;

using SimpleBlog.Entities.Dtos;
using SimpleBlog.Services.Constracts;

using Microsoft.AspNetCore.Mvc;

namespace SimpleBlog.Controllers
{
    [Route("lien-he")]
    public class ContactController : Controller
    {
        private readonly ICustomerService _customerService;
        private readonly ICategoryService _CategoryService;

        public ContactController(ICustomerService customerService, ICategoryService CategoryService)
        {
            _customerService = customerService;
            _CategoryService = CategoryService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            ViewBag.Categorys = _CategoryService.GetCategoryByOperation().Result.Select(s => new { s.Id, Value = s.Title }).ToList();
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> SubmitQuestion(CustomerQuestion customer)
        {
            if (ModelState.IsValid)
            {
                var custoerAsync = await _customerService.AddCustomerAsync(customer.CustomerDto);
            }

            ModelState.Clear();
            return View("Index");
        }
    }
}