﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using SimpleBlog.Entities;
using SimpleBlog.Entities.Dtos;
using SimpleBlog.Services.Constracts;
using SimpleBlog.Infastructure.Helpers;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace SimpleBlog.Controllers
{
    [Route("/bai-viet-moi-nhat")]
    public class PostController : Controller
    {
        private readonly IOptions<BlogSetting> config;
        private readonly IPostService _newService;
        public PostController(IPostService newsService, IOptions<BlogSetting> config)
        {
            _newService = newsService;
            this.config = config;
        }
        [HttpGet("")]
        public async Task<IActionResult> Index([FromQuery]int page = 0, int categoryId = 0)
        {
            int pageSize = config.Value.PageSize;
            PaginatedItems<PostDto> result = null;
            if (categoryId == 0)
                result = await _newService.GetPosts(page, pageSize);
            else
                result = await _newService.GetPostByCategoryId(categoryId, page, pageSize);
            ViewData["MaxPage"] = Math.Ceiling((double)result.Count / pageSize);
            ViewData["prev"] = result.PageIndex + 1;
            ViewData["next"] = page <= 1 ? 0 : page - 1;
            return View((List<PostDto>)result.Data);
        }
        [HttpGet("chi-tiet")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var news = await _newService.GetPostById(id.Value);
            if (news == null)
            {
                return NotFound();
            }
            return View(news);
        }
    }
}