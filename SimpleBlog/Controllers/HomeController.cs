﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;

using SimpleBlog.Models;
using SimpleBlog.Services.Constracts;

namespace SimpleBlog.Controllers
{
    [Route("")]
    public class HomeController : Controller
    {
        private readonly ICategoryService _CategoryService;
        private readonly IPostService _newsService;
        public HomeController(ICategoryService CategoryService, IPostService newsService)
        {
            _CategoryService = CategoryService;
            _newsService = newsService;
        }

        public async Task<IActionResult> Index()
        {
            var fieldOfCategories = await _CategoryService.GetCategoryByOperation();
            var news = await _newsService.GetPosts();
            ViewData["posts"] = news.OrderByDescending(x => x.UpdatedDate).Take(3);
            ViewData["Category"] = fieldOfCategories.Take(4);
            return View();
        }
        [HttpGet("about")]
        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";
            return View();
        }
       
        [HttpGet("error")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost]
        public IActionResult SetLanguage(string culture, string returnUrl)
        {
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );

            return LocalRedirect(returnUrl);
        }
    }
}
