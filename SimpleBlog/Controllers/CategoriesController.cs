﻿using System.Collections.Generic;

using SimpleBlog.Entities.Dtos;
using SimpleBlog.Services.Constracts;

using Microsoft.AspNetCore.Mvc;

namespace SimpleBlog.Controllers
{
    [Route("chu-de")]
    public class CategoriesController : Controller
    {
        private readonly ICategoryService _CategoryService;
        IEnumerable<CategoryDto> CategoryDtos;
        public CategoriesController(ICategoryService CategoryService)
        {
            _CategoryService = CategoryService;
            CategoryDtos =  _CategoryService.GetCategoryByOperation().Result;
        }
        
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }
    }
}
