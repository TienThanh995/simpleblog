﻿$(document).ready(function () {
    var url = "/Admin/Category/Raw";
    var myTable = $('#categoryTable').DataTable({
        ajax: {
            "url": url,
            "dataSrc": "data",
            "type": "GET"
        },
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        columns: [
            { "data": "title" },
            { "data": "slug" },
            {
                "data": null,
                "className": "center",
                "defaultContent": '<button id="btn_edit" class="btn btn-primary">Sửa</button> | <button id = "btn_delete" class="btn btn-danger btn-ok">Xóa</button>'
            },
            { "data": "id" }
        ],
        columnDefs: [
            { "width": "40%", "targets": 0 },
            { "width": "40%", "targets": 1 },
            { "width": "20%", "targets": 2 },
            {
                targets: 3,
                visible:false
            }
        ]
    });
    var btn = '';
    var category = new Object();
    var rowIndex = '';
    var currentRow = new Object();
    var model = {
        id: $(".modal-body #id"),
        title: $(".modal-body #title"),
        slug: $(".modal-body #slug")
    };

    $('#btn_add').on('click', function () {
        btn = 'Create';
        model.id.val('');
        model.title.val('');
        model.slug.val('');
        $('#categoryModal').modal('toggle');
    });
    $('#categoryTable tbody').on('click', '#btn_edit', function () {
        btn = 'Update';
        var data = myTable.row($(this).parents('tr')).data();
        currentRow = $(this).parents('tr');
        model.title.val(data.title);
        model.slug.val(data.slug);
        model.id.val(data.id);
        $('#categoryModal').modal('toggle');
    });

    $('#btn_save').on('click', function () {
        category.Title = model.title.val();
        category.Slug = model.slug.val();
       
        var url = '';
        if (btn === 'Create') {
            category.Id = 0;
            url = "/Admin/Category/Create";
        } else {
            category.Id = model.id.val();
            url = "/Admin/Category/update?id=" + category.Id;         
        }

        $.ajax({
            type: "POST",
            url: url,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            data: JSON.stringify(category),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (btn === 'Create')
                    myTable.row.add(response).draw(false)
                else {
                    myTable.row(currentRow).data(response);
                }
            },
            failure: function (response) {
                alert(response);
            }
        });
    });

    $('#delete_action').on('click', function () {
        $.ajax({
            type: "DELETE",
            url: "Delete?id=" + rowIndex,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            data: "",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                myTable.row(currentRow).remove().draw(false);
            },
            failure: function (response) {
                alert(response);
            }
        });
    });

    $('#categoryTable tbody').on('click', '#btn_delete', function () {
        currentRow = $(this).parents('tr');
        var data = myTable.row($(this).parents('tr')).data();
        rowIndex = data.id;
        $('#confirm-delete').modal('toggle');
    });

});