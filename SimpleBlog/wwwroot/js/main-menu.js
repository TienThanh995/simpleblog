﻿$(document).ready(function () {
    // Set up datatable
    var _url = "/Admin/MainMenu/Raw";
    var myTable = $('#menuTable').DataTable({
        ajax: {
            "url": _url,
            "dataSrc": "data",
            "type": "GET"
        },
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        columns: [
            { "data": "title" },
            { "data": "status" },
            { "data": "link" },
            { "data": "target" },
            { "data": "displayOrder" },
            { "data": "isParent" },
            {
                "data": null,
                "className": "center",
                "defaultContent": '<button id="btn_edit" class="btn btn-primary">Sửa</button> | <button id = "btn_delete" class="btn btn-danger btn-ok">Xóa</button>'
            },
            { "data": "id" }
        ],
        columnDefs:
            [
                {
                    autoWidth: true,
                    orderable: false,
                    className: 'select-checkbox',
                    targets: 1,
                    render: function (data, type, full) {
                        if (data == "1") {
                            return '<input type=\"checkbox\" checked value="' + data + '">';
                        } else {
                            return '<input type=\"checkbox\" value="' + data + '">';
                        }
                    }
                },
                {
                    autoWidth: true,
                    orderable: false,
                    className: 'select-checkbox',
                    targets: 5,
                    render: function (data, type, full) {
                        if (data == "1") {
                            return '<input type=\"checkbox\" checked value="' + data + '">';
                        } else {
                            return '<input type=\"checkbox\" value="' + data + '">';
                        }
                    }
                },
                {
                    targets: 7,
                    visible: false
                }]
    });
    var btn = '';
    var menu = new Object();
    var rowIndex = '';
    var currentRow = new Object();
    var model = {
        title: $(".modal-body #title"),
        display: $(".modal-body #isDisplay"),
        order: $(".modal-body #order"),
        hadChild: $(".modal-body #hadChild"),
        link: $(".modal-body #link"),
        target: $(".modal-body #target")
    };

    $('#btn_add').on('click', function () {
        btn = 'Create';
        model.title.val('');
        model.display.prop("checked", false);
        model.link.val('');
        model.target.val('');
        model.order.val('');
        model.hadChild.prop("checked", false);
        $('#mainMenuModal').modal('toggle');
    });
    $('#menuTable tbody').on('click', '#btn_edit', function () {
        btn = 'Update';
        var data = myTable.row($(this).parents('tr')).data();
        rowIndex = data.id;
        model.title.val(data.title);
        model.display.prop("checked", data.status);
        model.order.val(data.displayOrder);
        model.link.val(data.link);
        model.target.val(data.target);
        model.hadChild.prop("checked", data.isParent);
        $('#mainMenuModal').modal('toggle');
    });

    $('#btn_save').on('click', function () {
        menu.Title = model.title.val();
        menu.DisplayOrder = model.order.val();
        menu.Status = model.display[0].checked;
        menu.IsParent = model.hadChild[0].checked;
        menu.Link = model.link.val();
        menu.Target = model.target.val();
        rowIndex ? menu.Id = rowIndex : menu.Id = 0;

        var url = '';
        btn === 'Create' ? url = "/Admin/MainMenu/Create" : url = "/Admin/MainMenu/update?id=" + rowIndex;

        $.ajax({
            type: "POST",
            url: url,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            data: JSON.stringify(menu),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                btn === 'Create' ? myTable.row.add(response).draw(false) : myTable.row(menu).data(response);
            },
            failure: function (response) {
                alert(response);
            }
        });
    });

    $('#delete_action').on('click', function () {
        $.ajax({
            type: "DELETE",
            url: "Delete?id=" + rowIndex,
            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            data: "",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                myTable.row(currentRow).remove().draw(false);
            },
            failure: function (response) {
                alert(response);
            }
        });
    });

    $('#menuTable tbody').on('click', '#btn_delete', function () {
        currentRow = $(this).parents('tr');
        var data = myTable.row($(this).parents('tr')).data();
        rowIndex = data.id;
        $('#confirm-delete').modal('toggle');
    });

});