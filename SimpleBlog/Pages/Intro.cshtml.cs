﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Localization;
using SimpleBlog.Services.Constracts;

namespace SimpleBlog.Pages
{
    public class IntroModel : PageModel
    {
        private readonly ICategoryService _categoryService;
        private readonly ICompanyInfoService _companyService;
        private readonly IStringLocalizer<IntroModel> _localizer;
        public IntroModel(ICategoryService categoryService, ICompanyInfoService companyService, IStringLocalizer<IntroModel> localizer)
        {
            _categoryService = categoryService;
            _companyService = companyService;
            _localizer = localizer;
        }
        public async Task OnGet()
        {
            ViewData["About_Us"] = _localizer["Về SimpleBlog"];
            ViewData["About_Description"] = "giúp bạn hiểu về SimpleBlog";
            var Categorys = await _categoryService.GetCategoryByOperation();
            ViewData["Categorys"] = Categorys.ToList();
            var com = await _companyService.GetFirstCompany();
        }
    }
}