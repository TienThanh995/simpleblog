﻿using AutoMapper;
using SimpleBlog.DataAccess.EFs;
using SimpleBlog.DataAccess.Repositorys;
using SimpleBlog.DataAccess.Repositorys.Constracts;
using SimpleBlog.Services;
using SimpleBlog.Services.Constracts;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SimpleBlog.Areas.Admin.Data;
using SimpleBlog.Areas.Admin.Models;
using SimpleBlog.Areas.Admin.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using System;
using ICustomerService = SimpleBlog.Services.Constracts.ICustomerService;
using SimpleBlog.Entities;
using Microsoft.AspNetCore.Mvc.Razor;
using System.Globalization;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.Options;
using SimpleBlog.Middlewares;

namespace SimpleBlog
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddLocalization(o =>
            {
                o.ResourcesPath = "Resources";
            });

            var conn = Configuration.GetConnectionString("SimpleBlogConnection");
            services.Configure<BlogSetting>(Configuration.GetSection("BlogSetting"));

            services.AddDbContext<SimpleBlogContext>(options =>
            {
                options.UseLazyLoadingProxies()
                .UseSqlServer(conn,
                    sqlServerOptions =>
                    {
                        sqlServerOptions.MigrationsAssembly("SimpleBlog.DataAccess");
                    });
            });
            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(conn,
                    sqlServerOptions =>
                    {
                        sqlServerOptions.MigrationsAssembly("SimpleBlog");
                    });
            });

            RegisterService(services);

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddAuthorization(options =>
            {
                options.AddPolicy("Admin",
                    authBuilder =>
                    {
                        authBuilder.RequireRole("Administrators");
                    });

            });
            // Add application services.
            services.AddTransient<IEmailSender, EmailSender>();
            services.AddAutoMapper();
            services.Configure<IISOptions>(options =>
            {
                options.ForwardClientCertificate = false;
            });

            services.AddMvc()
                .AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix)
                .AddDataAnnotationsLocalization()
                .AddRazorPagesOptions(options =>
                {
                    options.Conventions.AddPageRoute("/intro", "gioi-thieu");
                });
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme);
            services.AddAntiforgery(o => o.HeaderName = "XSRF-TOKEN");

            services.ConfigureApplicationCookie(options =>
            {
                options.AccessDeniedPath = "/Account/AccessDenied";
                options.Cookie.Name = "SimplBlogCookie";
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(60);
                options.LoginPath = "/Admin/Account/Login";
                options.ReturnUrlParameter = CookieAuthenticationDefaults.ReturnUrlParameter;
                options.SlidingExpiration = true;
            });
            services.Configure<RequestLocalizationOptions>(options =>
            {
                var supportedCultures = new[]
                {
                    new CultureInfo("en-US"),
                    new CultureInfo("vi-VN")
                };
                options.DefaultRequestCulture = new RequestCulture(culture: "vi-VN", uiCulture: "vi-VN");
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            var locOptions = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            app.UseRequestLocalization(locOptions.Value);
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
                app.UseExceptionMiddleware();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseExceptionMiddleware();
            }

            app.UseStaticFiles();
            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
            app.UseMvc(route =>
            {
                route.MapRoute(
                name: "areas",
                template: "{area:exists}/{controller=Home}/{action=Index}/{id?}"
);
            });
        }

        public void RegisterService(IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped(typeof(IPostService), typeof(PostService));
            services.AddScoped(typeof(ICustomerService), typeof(CustomerService));
            services.AddScoped(typeof(ICategoryService), typeof(CategoryService));
            services.AddScoped(typeof(IMainMenuService), typeof(MainMenuService));
            services.AddScoped(typeof(ICompanyInfoService), typeof(CompanyInfoService));
        }
    }
}
